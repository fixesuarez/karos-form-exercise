import Vue from 'vue'
import App from './App.vue'
import '../assets/app.css'

/* eslint-disable-next-line no-new */
new Vue({
  el: '#karos-form-exercise',
  render: h => h(App)
})
